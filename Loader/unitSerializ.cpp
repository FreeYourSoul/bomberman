//
// unitSerializ.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Core
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Mon May 13 16:06:04 2013 quentin balland
// Last update Tue May 14 22:32:25 2013 quentin balland
//

#include		<string>
#include		<sstream>
#include		<utility>
#include		"SerializeGame.hh"
#include		"../Core/AUnit.hpp"
#include		"../Core/BombManager.hpp"
#include		"../Map/AMapElement.hh"

void			AUnit::seriaIdPlayer(std::string& seria, const size_t id) const
{
  std::stringstream	ss;

  ss << "<c> " << id;
  ss << " </c>";
  SerializeGame::seria(seria,  "<idPlayer>",				SerializeGame::JUMP);
  SerializeGame::seria(seria,    ss.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,  "</idPlayer>",				SerializeGame::NOT);
}

void			AUnit::seriaCoord(std::string& seria,
					  const std::pair<int, int>& val) const
{
  std::stringstream	ss;
  std::stringstream	ss2;

  ss << "<c> " << (val).first;
  ss << " </c>";
  ss2 << "<c> " << (val).second;
  ss2 << " </c>";
  SerializeGame::seria(seria, "<coord>",				SerializeGame::JNOT);
  SerializeGame::seria(seria,   "<X>",					SerializeGame::JUMP);
  SerializeGame::seria(seria,   ss.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,   "</X>",					SerializeGame::NOT);
  SerializeGame::seria(seria,   "<Y>",					SerializeGame::JNOT);
  SerializeGame::seria(seria,   ss2.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,   "</Y>",					SerializeGame::NOT);
  SerializeGame::seria(seria, "</coord>",				SerializeGame::ERAS);
}


template<typename T>
void			AUnit::seriaStat(std::string& seria,
					 const std::string& balise,
					 const T val) const
{
  std::stringstream	ss;

  ss << "<" << balise << ">" << " <c> " << val;
  ss << " </c> </" << balise << ">";
  SerializeGame::seria(seria, ss.str(),					SerializeGame::JNOT);
}
template void		AUnit::seriaStat(std::string&, const std::string&, const int) const;
template void		AUnit::seriaStat(std::string&, const std::string&, const bool) const;
template void		AUnit::seriaStat(std::string&, const std::string&, const double) const;
template void		AUnit::seriaStat(std::string&, const std::string&, const AUnit::UnitType) const;

void			AUnit::seriaBombs(std::string& seria, const BombManager& manag) const
{
  std::stringstream	ss;
  std::stringstream	ss2;
  size_t		i;

  ss << "<nbBomb> <c> " << manag.getNbBombs();
  ss << " </c> </nbBomb>";
  ss2 << "<idBomb> <c> " << manag.getTypeBomb();
  ss2 << " </c> </idBomb>";
  SerializeGame::seria(seria, "<bomb>",					SerializeGame::JNOT);
  SerializeGame::seria(seria,    ss2.str(),				SerializeGame::JUMP);
  SerializeGame::seria(seria,    ss.str(),				SerializeGame::JNOT);
  SerializeGame::seria(seria, "</bomb>",				SerializeGame::ERAS);
}

std::string		AUnit::serialize() const
{
  std::string		seria("<_player>");

  seriaIdPlayer(seria, this->_id);
  seriaCoord(seria, this->_position);
  seriaStat<int>(seria, "shield", this->_shield);
  seriaStat<bool>(seria, "isAlive", this->_alive);
  seriaStat<double>(seria, "moveSpeed", this->_movespeed);
  seriaStat<double>(seria, "typePlayer", this->_type);
  seriaBombs(seria, *this->_manager);
  SerializeGame::seria(seria, "</_player>",				SerializeGame::ERAS);
  return (seria);
}
