//
// SerializeGame.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Fri May 10 01:36:11 2013 quentin balland
// Last update Tue May 14 10:54:08 2013 quentin balland
//

#ifndef			SERIALIZEGAME_HH_
# define		SERIALIZEGAME_HH_

# include		<vector>
# include		"ISerializable.hh"

class			SerializeGame
{
  // ERAS = jump and ERASe 2 space,
  // JUMP = JUMP and add 2 space,
  // JUNO = JUmp and do NOt add or erase 2 space
  // NOT  = Do not jump and ignore the actual incrementation of space
  // JINC = just increm for after
  // JDEC = just decrem for after
public:
  enum			inc
    {
      ERAS,
      JUMP,
      JNOT,
      NOT,
      JINC,
      JDEC
    };

private:
  std::string		saveFileName;
  std::string		seriaContent;

public:
  ~SerializeGame();
  SerializeGame(const std::string&);

  std::string		launchSerializeGame(const std::vector<ISerializable *>&);
  static void		seria(std::string&, const std::string&, const SerializeGame::inc);

};

#endif // !SERIALIZEGAME_HH_
