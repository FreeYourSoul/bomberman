//
// DeSerializeGame.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Sun May 12 22:07:04 2013 quentin balland
// Last update Mon May 20 19:22:05 2013 quentin balland
//

#ifndef			DESERIALIZEGAME_HH_
# define		DESERIALIZEGAME_HH_

# include		<string>
# include		<vector>
# include		<map>
# include		"../ParserXML/ParserXML.hh"
# include		"../Map/Bonus/ABonus.hh"
# include		"../Map/Map.hh"
# include		"../Map/Box.hh"
# include		"../Core/AUnit.hpp"
# include		"../Core/ABomb.hpp"

class			DeSerializeGame
{

private:
  std::string			fileSaveName;
  std::map<int, AUnit *>	players;
  Map				*mapSave;

public:
  ~DeSerializeGame();
  DeSerializeGame(const std::string&);

private:
  void			createSaveDTD() const;
  void			deserializeMap(const Parser::Node&);
  void			deserializePlayers(const std::vector<Parser::Node>&);
  Box			*getElemBox(const int) const;
  ABomb			*getElemBomb(const int) const;
  ABonus		*getElemBonus(const int) const;
  Parser::Node		getPrincipalNode(const Parser::Node&) const;
  void			fillMapElement(t_map&, const std::vector<Parser::Node>&) const;


public:
  std::string			getSaveName() const;
  std::map<int, AUnit *>	getPlayers() const;
  Map				*getMap() const;
  void				setSaveName(const std::string&);

};

#endif // !DESERIALIZEGAME_HH_
