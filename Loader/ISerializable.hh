//
// ISerializable.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Fri May 10 01:33:42 2013 quentin balland
// Last update Tue May 14 18:32:15 2013 quentin balland
//

#ifndef		ISERIALIZABLE_HH_
# define	ISERIALIZABLE_HH_

# include	<string>

class		ISerializable
{

public:
  virtual ~ISerializable() {};

  virtual std::string	serialize() const = 0;

};

#endif // !ISERIALIZABLE_HH_
