//
// DeSerializeGame.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Sun May 12 22:07:52 2013 quentin balland
// Last update Mon May 20 19:46:48 2013 quentin balland
//

#include <iostream>
#include <utility>
#include <vector>
#include "DeSerializeGame.hh"
#include "../ParserXML/ParserXML.hh"
#include "../ParserXML/ParserXMLException.hh"
#include "../Map/Bonus/ABonus.hh"
#include "../Map/Box.hh"
#include "../Map/Map.hh"
#include "../Core/AUnit.hpp"
#include "../Core/ABomb.hpp"

DeSerializeGame::~DeSerializeGame()
{
  delete (mapSave);
}

DeSerializeGame::DeSerializeGame(const std::string& saveName) :
  fileSaveName(saveName)
{
  Parser::ParserXML	parser(saveName, "save.dtd");
  Parser::Node		principalNode;

  principalNode = getPrincipalNode(parser.getTreeXML());
  deserializeMap(principalNode.getChildren("map").at(0));
  deserializePlayers(principalNode.getChildren("players"));
}

Parser::Node			DeSerializeGame::getPrincipalNode(const Parser::Node& tree) const
{
  std::vector<Parser::Node>	tmp;

  tmp = tree.getChildren("game");
  if (tmp.size() != 1 || tree.getNodeInsideSize() != 1)
    throw ParserXMLException("Error in save.xml format: no or too many open balise (game).");
  return (tmp.at(0));
}

Box		*DeSerializeGame::getElemBox(const int idElem) const
{
  return (NULL);
}

ABomb		*DeSerializeGame::getElemBomb(const int idElem) const
{
  return (NULL);
}

ABonus		*DeSerializeGame::getElemBonus(const int idElem) const
{
  return (NULL);
}

void		DeSerializeGame::fillMapElement(t_map& elem, const std::vector<Parser::Node>& elm) const
{
  std::vector<AMapElement *>	mapElems;
  std::pair<int, int>		cElem;
  Parser::Node			cNod;
  Parser::Node			tNod;
  int				typeElem;

  for (std::vector<Parser::Node>::const_iterator it = elm.begin() ; it != elm.end() ; ++it)
    {
      typeElem = Parser::ParserXML::getNumContent((*it).getChildren("typeElem").at(0));
      cNod = (*it).getChildren("coord").at(0);
      tNod = (*it).getChildren("typeElem").at(0).getChildren("typeId").at(0);
      cElem = std::make_pair(Parser::ParserXML::getNumContent(cNod.getChildren("X").at(0)),
			     Parser::ParserXML::getNumContent(cNod.getChildren("Y").at(0)));
      if (typeElem == AMapElement::BOX)
	mapElems.push_back(getElemBox(Parser::ParserXML::getNumContent(tNod)));
      else if (typeElem == AMapElement::BONUS)
	mapElems.push_back(getElemBonus(Parser::ParserXML::getNumContent(tNod)));
      else if (typeElem == AMapElement::BOMBE)
	mapElems.push_back(getElemBomb(Parser::ParserXML::getNumContent(tNod)));
      elem[cElem] = mapElems;
    }
}

void				DeSerializeGame::deserializeMap(const Parser::Node& mapPars)
{
  Parser::Node			szNod;
  std::pair<int, int>		sizeMap;
  t_map				mapping;
  
  szNod = mapPars.getChildren("sizeMap").at(0);
  sizeMap = std::make_pair(Parser::ParserXML::getNumContent(szNod.getChildren("X").at(0)),
			   Parser::ParserXML::getNumContent(szNod.getChildren("Y").at(0)));
  fillMapElement(mapping, mapPars.getChildren("_elem"));
  std::cout << mapping.at(std::make_pair(0, 0)).at(0)->isDestructible() << std::endl;
  this->mapSave = new Map(mapping, sizeMap);
}

void			DeSerializeGame::deserializePlayers(const std::vector<Parser::Node>& playPars)
{
  
}

void			DeSerializeGame::createSaveDTD() const
{

}

std::map<int, AUnit *>	DeSerializeGame::getPlayers() const
{
  return (this->players);
}

Map			*DeSerializeGame::getMap() const
{
  return (this->mapSave);
}

std::string		DeSerializeGame::getSaveName() const
{
  return (this->fileSaveName);
}

void			DeSerializeGame::setSaveName(const std::string& save)
{
  this->fileSaveName = save;
}
