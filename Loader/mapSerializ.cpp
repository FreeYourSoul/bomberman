//
// mapSerializ.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Fri May 10 10:20:06 2013 quentin balland
// Last update Mon May 20 19:38:26 2013 quentin balland
//

#include		<utility>
#include		<sstream>
#include		"SerializeGame.hh"
#include		"../Map/Map.hh"

void			Map::seriaSizeOrCoord(std::string& seria,
					      const bool b,
					      const std::pair<int, int>& val) const
{
  std::stringstream	ss;
  std::stringstream	ss2;

  ss << "<c> " << (val).first;
  ss << " </c>";
  ss2 << "<c> " << (val).second;
  ss2 << " </c>";
  SerializeGame::seria(seria, ((b) ? "<sizeMap>" : "<coord>"),		SerializeGame::JUMP);
  SerializeGame::seria(seria,   "<X>",					SerializeGame::JUMP);
  SerializeGame::seria(seria,   ss.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,   "</X>",					SerializeGame::NOT);
  SerializeGame::seria(seria,   "<Y>",					SerializeGame::JNOT);
  SerializeGame::seria(seria,   ss2.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,   "</Y>",					SerializeGame::NOT);  
  SerializeGame::seria(seria, ((b) ? "</sizeMap>" : "</coord>"),	SerializeGame::ERAS);
}

void			Map::seriaElem(std::string& seria, const AMapElement *elem) const
{
  std::stringstream	ss;
  std::stringstream	ssType;
  std::stringstream	ssBool;

  ss << elem->getType();
  ssBool << elem->isDestructible();
  ssType << elem->getTypeElem();
  SerializeGame::seria(seria,  "<typeElem>",				SerializeGame::JNOT);
  SerializeGame::seria(seria,    "<c>",					SerializeGame::JUMP);
  SerializeGame::seria(seria,    ss.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,    "</c>",				SerializeGame::NOT);
  SerializeGame::seria(seria,    "<isDestructible>",			SerializeGame::JNOT);
  SerializeGame::seria(seria,    "<c>",					SerializeGame::NOT);
  SerializeGame::seria(seria,    ssBool.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,    "</c>",				SerializeGame::NOT);
  SerializeGame::seria(seria,    "</isDestructible>",			SerializeGame::NOT);
  SerializeGame::seria(seria,    "<typeId>",				SerializeGame::JNOT);
  SerializeGame::seria(seria,    "<c>",					SerializeGame::NOT);
  SerializeGame::seria(seria,    ssType.str(),				SerializeGame::NOT);
  SerializeGame::seria(seria,    "</c>",				SerializeGame::NOT);
  SerializeGame::seria(seria,    "</typeId>",				SerializeGame::NOT);
  SerializeGame::seria(seria,  "</typeElem>",				SerializeGame::ERAS);
}

void			Map::seriaMap(std::string& seria) const
{
  for (t_map::const_iterator it = _map.begin() ; it != _map.end() ; ++it)
    {
      for (std::vector<AMapElement *>::const_iterator itElem = it->second.begin() ;
	   itElem != it->second.end() ; ++itElem)
	{
	  SerializeGame::seria(seria, "<_elem>",			SerializeGame::JNOT);
	  seriaSizeOrCoord(seria, false, it->first);
	  seriaElem(seria, *itElem);
	  SerializeGame::seria(seria, "</_elem>",			SerializeGame::ERAS);
	}
    }  
}

#include <iostream>
std::string		Map::serialize() const
{
  std::string		seria("<map>");

  std::cout << _map.size() << std::endl;
  seriaSizeOrCoord(seria, true, this->_size);
  seriaMap(seria);
  SerializeGame::seria(seria, "</map>",					SerializeGame::ERAS);
  return (seria);
}
