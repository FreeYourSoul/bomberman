//
// Loader.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Tue May 14 21:46:53 2013 quentin balland
// Last update Tue May 14 21:55:51 2013 quentin balland
//

#include "Loader.hh"
#include "../Core/Game.hpp"

Loader::~Loader()
{}

Loader::Loader()
{}

Game			Loader::loadSaveGame(const std::string& saveFileName) const
{
  DeSerializeGame	deserialize(saveFileName);
  Game			newGame();

  return (newGame);
}

void			Loader::saveGame(const std::string& saveFileName, const Game& game) const
{
  
}
