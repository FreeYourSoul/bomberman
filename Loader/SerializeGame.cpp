//
// SerializeGame.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Fri May 10 01:40:29 2013 quentin balland
// Last update Tue May 14 18:57:15 2013 quentin balland
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "SerializeGame.hh"
#include "ISerializable.hh"
#include "../ParserXML/ParserXMLException.hh"

SerializeGame::~SerializeGame()
{}

SerializeGame::SerializeGame(const std::string& fileName) :
  saveFileName(fileName),
  seriaContent("")
{}

std::string		SerializeGame::launchSerializeGame(const std::vector<ISerializable *>& s)
{
  std::ofstream		file(saveFileName.c_str(), std::ios::out | std::ios::trunc);

  if (file)
    {
      SerializeGame::seria(seriaContent, "<game>",				JNOT);
      SerializeGame::seria(seriaContent, "",					JINC);
      for (std::vector<ISerializable *>::const_iterator it = s.begin() ; it != s.end() ; ++it)
	{
	  SerializeGame::seria(seriaContent, (*it)->serialize(),		JNOT);
	}
      SerializeGame::seria(seriaContent, "</game>",				ERAS);
      file << seriaContent;      
    }
  else
    throw ParserXMLException("Error in file creation.");
  file.close();
  return (seriaContent);
}

void			SerializeGame::seria(std::string& seria,
					     const std::string& cont,
					     const SerializeGame::inc bl)
{
  static std::string	inc("");

  if (bl == JINC || bl == JDEC)
    inc = (bl == JINC) ? (inc + "  ") : inc.substr(2);
  else
    {
      if (bl == ERAS)
	inc = inc.substr(2);
      inc +=  (bl == NOT || bl == JNOT) ? "" : ((bl == JUMP) ? "  " : "");
      seria += ((bl != NOT) ? ("\n" +  inc) : " ") + cont;
    }
}
