//
// Loader.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Loader
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Tue May 14 21:46:50 2013 quentin balland
// Last update Tue May 14 21:55:26 2013 quentin balland
//

#ifndef				LOADER_HH_
# define			LOADER_HH_

# include			<string>
# include			"DeSerializeGame.hh"
# include			"SerializeGame.hh"
# include			"../Core/Game.hpp"

class				Loader
{

public:
  ~Loader();
  Loader();

public:
  void				loadSaveGame(const std::string&) const;
  Game				saveGame(const std::string&, const Game&) const;

};

#endif // !LOADER_HH_
