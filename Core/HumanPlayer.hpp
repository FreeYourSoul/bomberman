//
// HumanPlayer.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 15:32:57 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:55:54 2013 lucas maillardet
//

#ifndef			HUMANPLAYER_HPP__
#define			HUMANPLAYER_HPP__

#include		"AUnit.hpp"

#include		<iostream>
#include		<utility>

class			HumanPlayer : public AUnit
{
private:

public:
  HumanPlayer(const int id);
  virtual ~HumanPlayer();

};

#endif	// !HUMANPLAYER_HPP__
