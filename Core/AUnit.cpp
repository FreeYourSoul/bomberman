//
// AUnit.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Tue May  7 10:38:59 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 11:40:49 2013 Pierre-Nicolas Sormani
//

#include		"../Loader/ISerializable.hh"
#include		"../Map/Bonus/ABonus.hh"
#include		"../Map/AMapElement.hh"
#include		"../Map/Map.hh"
#include		"BombManager.hpp"
#include		"Direction.hpp"
#include		"BombType.hpp"
#include		"ABomb.hpp"
#include		"AUnit.hpp"

AUnit::AUnit(UnitType type, size_t id, const std::pair<int, int>& pos) :
  AMapElement(AMapElement::PLAYER, pos),
  ISerializable(),
  _type(type),
  _manager(id),
  _id(id)
{
  _movespeed = 0.5;
  _shield = 0;
  _alive = true;
  _position = std::make_pair(pos.first, pos.second);
  _moveTab[RIGHT] = &AUnit::_moveRight;
  _moveTab[BOT] = &AUnit::_moveBot;
  _moveTab[LEFT] = &AUnit::_moveLeft;
  _moveTab[TOP] = &AUnit::_moveTop;
}

AUnit::AUnit(const UnitType type, const BombManager& bm, const size_t id,
	     const double ms, const int shield, const bool status,
	     const std::pair<int, int>& pos) :
  AMapElement(AMapElement::PLAYER, std::make_pair(pos.first, pos.second)),
  ISerializable(),
  _manager(bm)
{
  _type = type;
  _id = id;
  _movespeed = ms;
  _shield = shield;
  _alive = status;
}

AUnit::AUnit(const AUnit& other) :
  AMapElement(AMapElement::PLAYER, std::make_pair(other._position.first, other._position.second)),
  _manager(other._manager)
{
  _type = other._type;
  _id = other._id;
  _movespeed = other._movespeed;
  _shield = other._shield;
  _alive = other._alive;
}

AUnit&			AUnit::operator=(const AUnit& other)
{
  if (this != &other)
    {
      _type = other._type;
      _manager = (other._manager);
      _id = other._id;
      _movespeed = other._movespeed;
      _shield = other._shield;
      _alive = other._alive;
      _position = std::make_pair(other._position.first, other._position.second);
    }
  return (*this);
}

bool			AUnit::addBomb()
{
  _manager.addBomb();
  return (true);
}

bool			AUnit::changeBomb(const ABomb& bomb)
{
  _manager.changeBomb(bomb);
  return true;
}

bool			AUnit::addRangeBomb()
{

  return true;
}

bool			AUnit::addPowerBomb()
{
  return true;
}

bool			AUnit::addMoveSpeed()
{
  if (_movespeed == 6)
    return false;
  _movespeed += 0.25;
  return true;
}

bool			AUnit::addShield()
{
  if (_shield == 10)
    return false;
  ++_shield;
  return true;
}

bool			AUnit::putBomb(std::list<ABomb*>& list)
{
  ABomb*		bomb = NULL;

  bomb = _manager.putBomb(getPosition());
  if (!bomb)
    return false;

  /*
  ** OUBLIE PAS DE SET LE TIMER !!
  */

  bomb->setTimer(30);
  list.push_back(bomb);
  return true;
}

bool			AUnit::pushBomb()
{
  return true;
}

bool			AUnit::touched()
{
  if (_shield > 0)
    --_shield;
  else
    _alive = false;
  return !_alive;
}

std::string		AUnit::serialize() const
{
  return "test";
}

bool			AUnit::_checkElement(Map& map, const std::pair<int, int>& pos)
{
  std::vector<AMapElement*>	list;

  list = map.getElementOnPos(pos);
  for (std::vector<AMapElement*>::iterator it = list.begin() ; it != list.end() ; ++it)
    {
      if (*it)
	{
	  if ((*it)->getType() == AMapElement::BOX || (*it)->getType() == AMapElement::BOMBE)
	    return false;
	}
    }
  return true;
}

bool			AUnit::_moveRight(Map& map)
{
  std::pair<int, int>	pos;

  pos = getPosition();
  if (pos.first == map.getSize().first)
    return false;
  pos.second += 1;
  return _checkElement(map, pos);
}

bool			AUnit::_moveBot(Map& map)
{
  std::pair<int, int>	pos;

  pos = getPosition();
  if (pos.second == map.getSize().second)
    return false;
  pos.second += 1;
  return _checkElement(map, pos);
}

bool			AUnit::_moveLeft(Map& map)
{
  std::pair<int, int>	pos;

  pos = getPosition();
  if (pos.first == 0)
    return false;
  pos.second -= 1;
  return _checkElement(map, pos);
}

bool			AUnit::_moveTop(Map& map)
{
  std::pair<int, int>	pos;

  pos = getPosition();
  if (pos.second == 0)
    return false;
  pos.second -= 1;
  return _checkElement(map, pos);
}

bool			AUnit::move(const Direction dir, Map& map)
{
  if (dir > TOP || dir < 0)
    return false;
  return (this->*_moveTab[dir])(map);
}
