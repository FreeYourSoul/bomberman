//
// Game.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 14:15:03 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 11:39:55 2013 Pierre-Nicolas Sormani
//

#include		"../Map/Map.hh"
#include		"HumanPlayer.hpp"
#include		"AI.hpp"
#include		"AUnit.hpp"
#include		"ABomb.hpp"
#include		"Game.hpp"

Game::Game(const int x, const int y, int humanPlayers, int ai)
  : _map(x, y)
{
  int			id = 0;
  AUnit*		unit;

  while (id < humanPlayers)
    {
      unit = new HumanPlayer(id);		// creation et
      _players.push_back(unit);			// push of players on the list
      ++id;
    }
  for (int i = 0 ; i < ai ; ++i)
    {
      unit = new AI(id);			// creation et
      _players.push_back(unit);			// push of players on the list
      ++id;
    }
  _bombs.clear();
  _killed.clear();
}

Game::~Game()
{}

void			Game::launch()
{
  _explosions();
}

/*
** PRIVATE functions
*/

void			Game::_explosions()
{
  for (std::list<ABomb*>::iterator it = _bombs.begin() ;
       it != _bombs.end() ; ++it)
    (*it)->explode(_map, _players, _killed);
  for (std::list<ABomb*>::iterator it = _bombs.begin() ;
       it != _bombs.end() ; )
    {
      if ((*it)->getExplode() == true)
	{
	  delete *it;
	  it = _bombs.erase(it);
	}
      else
	++it;
    }
}

void			Game::move(AUnit* unit, const Direction dir)
{
  if (unit->move(dir, _map))
    {
      _map.movement(unit, dir);
    }
}
