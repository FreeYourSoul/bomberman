//
// StandBomb.hpp for  in /home/sorman/Epitech/Tek2/c++/mybbman/Bombs
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Thu May  9 16:41:10 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:33:30 2013 Pierre-Nicolas Sormani
//

#ifndef			STANDARDBOMB_HPP__
#define			STANDARDBOMB_HPP__

#include		"../BombType.hpp"

class			ABomb;
class			Map;

class			StandardBomb : public ABomb
{
private:

  public:
  StandardBomb(const std::pair<int, int>&, const size_t playerID);
  StandardBomb(const StandardBomb&);
  virtual ~StandardBomb() {}

  StandardBomb*		clone(const std::pair<int, int>&) const;
  void			explode(Map& map, std::vector<AUnit*>& players,
				std::list<AUnit*>& killed);
  int			_doExplosion(Map& map, const std::pair<int, int>& pos,
				     std::vector<AUnit*>& players, std::list<AUnit*>& killed);
};

#endif	// !STANDARDBOMB_HPP_
