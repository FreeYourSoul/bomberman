//
// StandardBomb.cpp for  in /home/sorman/Epitech/Tek2/c++/mybbman/Bombs
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Thu May  9 16:42:47 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:33:37 2013 Pierre-Nicolas Sormani
//

#include		"../ABomb.hpp"
#include		"StandardBomb.hpp"
#include		"../../Map/Map.hh"
#include		"../../Map/Box.hh"
#include		"../../Map/Bonus/ABonus.hh"

StandardBomb::StandardBomb(const std::pair<int, int>&pos, const size_t playerID)
  : ABomb(pos, STANDARD, playerID, 2, 1)
{}

StandardBomb::StandardBomb(const StandardBomb& other)
  : ABomb(other._position, other.getType(), other.getPlayerID(), other.getRange(), other.getPower())
{}

StandardBomb*			StandardBomb::clone(const std::pair<int, int>& pos) const
{
  StandardBomb*			bomb;

  bomb = new StandardBomb(pos, _playerID);
  return bomb;
}

void				StandardBomb::explode(Map& map, std::vector<AUnit*>& players,
						      std::list<AUnit*>& killed)
{
  int				i = 1;
  const std::pair<int, int>	len = map.getSize();
  std::pair<int, int>		pos;
  int				right, left, top, bot;

  if (_explode == true)
    return ;
  _explode = true;
  (players[_playerID])->addBomb();
  right = 0;
  left = 0;
  top = 0;
  bot = 0;
  i = 1;
  while (static_cast<unsigned int>(i) < _range)
    {
      /*
      ** toTheRight
      */
      pos.first = _position.first + i;
      pos.second = _position.second;
      if (pos.first < len.first && static_cast<unsigned int>(right) < _power)
	{
	  right += _doExplosion(map, pos, players, killed);
	}

      /*
      ** toTheTop
      */
      pos.first = _position.first;
      pos.second = _position.second + i;
      if (pos.second < len.second && static_cast<unsigned int>(top) < _power)
	{
	  top += _doExplosion(map, pos, players, killed);
	}

      /*
      ** toTheLeft
      */
      pos.first = _position.first - i;
      pos.second = _position.second;
      if (pos.first >= 0 && static_cast<unsigned int>(left) < _power)
	{
	  left += _doExplosion(map, pos, players, killed);
	}

      /*
      ** toTheBot
      */
      pos.first = _position.first;
      pos.second = _position.second - i;
      if (pos.second >= 0 && static_cast<unsigned int>(bot) < _power)
	{
	  bot += _doExplosion(map, pos, players, killed);
	}
      ++i;
    }
}

int				StandardBomb::_doExplosion(Map& map,
							   const std::pair<int, int>& pos,
							   std::vector<AUnit*>& players,
							   std::list<AUnit*>& killed)
{
  std::vector<AMapElement*>	mapElmts;
  int				val;

  val = 0;
  mapElmts = map.getElementOnPos(pos);
  for (std::vector<AMapElement*>::iterator it = mapElmts.begin() ;
       it != mapElmts.end() ; )
    {
      if (*it)
	{
	  if ((*it)->getType() == AMapElement::PLAYER)
	    {
	      AUnit*		unit;

	      unit = reinterpret_cast<AUnit*>(*it);
	      if (unit->isAlive() && unit->touched() == true)
		killed.push_back(unit);
	    }
	  if ((*it)->getType() == AMapElement::BOMBE)
	    {
	      ABomb*		bomb;

	      bomb = reinterpret_cast<ABomb*>(*it);
	      bomb->explode(map, players, killed);
	    }
	  if ((*it)->getType() == AMapElement::BOX)
	    {
	      Box*		box;

	      box = reinterpret_cast<Box*>(*it);
	      if (box->isDestructible() == false)
		return _power;
	      else
		val = 1;
	      AMapElement*	elmt;

	      elmt = box->explode();
	      if (elmt)
		{
		  elmt->setPosition(pos);
		  map.putElement(elmt);
		}
	    }
	}
      ++it;
    }
  return val;
}
