//
// AI.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 17:24:36 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:31:56 2013 lucas maillardet
//

#include		"AUnit.hpp"
#include		"AI.hpp"

AI::AI(const int id) :
  AUnit(AUnit::AI, id, std::make_pair(-1, -1))
{
  std::cout << AUnit::getID() << " - A AI has been created." << std::endl;
}

AI::~AI()
{}
