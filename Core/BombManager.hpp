//
// BombManager.hpp for  in /home/sorman/Epitech/Tek2/c++/mybbman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Thu May  9 16:24:56 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:44:29 2013 Pierre-Nicolas Sormani
//

#ifndef			BOMBMANAGER_HPP__
#define			BOMBMANAGER_HPP__

#include		"BombType.hpp"

class			ABomb;

class			BombManager
{
  BombType		_typeBomb;
  int			_nbBombs;
  ABomb*		_bomb;

public:
  BombManager(const int id, const BombType type = STANDARD);
  BombManager(const BombManager&);
  BombManager&		operator=(const BombManager&);

  virtual ~BombManager();

  BombType		getTypeBomb() const {return (_typeBomb);};
  int			getNbBombs() const {return (_nbBombs);};
  ABomb			*putBomb(const std::pair<int, int>&);
  bool			changeBomb(const ABomb&);

  void			addBomb();
  bool			addRangeBomb();
  bool			addPowerBomb();

};

#endif	// !BOMBMANAGER_HPP__
