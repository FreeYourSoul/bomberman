//
// ABomb.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May  3 15:11:29 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:22:11 2013 Pierre-Nicolas Sormani
//

#include		"../Map/AMapElement.hh"
#include		"ABomb.hpp"

ABomb::~ABomb()
{}

ABomb::ABomb(const std::pair<int, int>& pos, BombType type, size_t playerID, size_t range,
	     size_t power)
  : AMapElement(AMapElement::BOMBE, pos), _type(type), _range(range),
    _power(power), _playerID(playerID), _timer(-1), _explode(false)

{}

ABomb::ABomb(const ABomb& other) : AMapElement(AMapElement::BOMBE, other._position)
{
  _type = other._type;
  _range = other._range;
  _power = other._power;
  _playerID = other._playerID;
  _timer = other._timer;
  _explode = other._explode;
}

/*
** Operators
*/

ABomb&			ABomb::operator=(const ABomb& other)
{
  _type = other._type;
  _range = other._range;
  _power = other._power;
  _playerID = other._playerID;
  _timer = other._timer;
  _explode = other._explode;
  return *this;
}

/*
** Setters
*/

void			ABomb::setRange(size_t i) {
  _range += i;
}

void			ABomb::setPower(size_t i) {
  _power += i;
}

void			ABomb::setTimer(const float& timer) {
  if (timer >= 0)
    _timer = timer;
}

/*
** Operations
*/

void			ABomb::addRange() {
  ++_range;
}

void			ABomb::addPower() {
  ++_power;
}
