//
// AI.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 15:36:19 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:23:49 2013 lucas maillardet
//

#ifndef			AI_HPP__
#define			AI_HPP__

#include		<iostream>
#include		<utility>

class			AUnit;

class			AI : public AUnit
{
public:

  AI(const int id);
  virtual ~AI();

};


#endif	// !AI_HPP__
