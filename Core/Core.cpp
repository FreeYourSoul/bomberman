//
// Core.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 14:06:15 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:28:18 2013 lucas maillardet
//

#include		"Game.hpp"
#include		"Core.hpp"

Core::Core()
{}

Core::~Core()
{}

void			Core::run()
{
  int			players = 1;
  int			AI = 10;
  int			x = 25;
  int			y = 25;

  _game = new Game(x, y, players, AI);
  _game->launch();
}
