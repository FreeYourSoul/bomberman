//
// main.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 16:52:51 2013 Pierre-Nicolas Sormani
// Last update Fri May 17 17:22:40 2013 Pierre-Nicolas Sormani
//

#include	"Core.hpp"
#include	"HumanPlayer.hpp"

int	main()
{
  Core	c;

  c.run();
  return 0;
}
