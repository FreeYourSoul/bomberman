//
// Direction.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Tue May 21 11:03:35 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 11:51:55 2013 lucas maillardet
//

#ifndef			DIRECTION_HPP__
#define			DIRECTION_HPP__

enum			Direction
  {
    RIGHT = 0,
    BOT,
    LEFT,
    TOP,
    ENDDIR
  };

#endif	// !DIRECTION_HPP__
