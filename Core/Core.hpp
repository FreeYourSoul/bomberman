//
// Core.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 14:06:21 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:21:23 2013 lucas maillardet
//

#ifndef			CORE_HPP__
#define			CORE_HPP__

class			Game;

class			Core
{
private:
  Game*			_game;

public:
  Core();
  ~Core();

  void			run();
};

#endif	// !CORE_HPP__
