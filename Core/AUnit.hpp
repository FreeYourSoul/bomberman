//
// AUnit.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Tue May  7 10:39:05 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 11:21:22 2013 Pierre-Nicolas Sormani
//

#ifndef			AUNIT_HPP__
# define		AUNIT_HPP__

# include		<utility>
# include		<string>
# include		<list>

# include		"../Map/AMapElement.hh"
# include		"../Loader/ISerializable.hh"
# include		"BombManager.hpp"
# include		"Direction.hpp"

class			ABonus;
class			ABomb;
class			Map;

class			AUnit : public AMapElement, public ISerializable
{
public:

  typedef bool		(AUnit::*moveFunc)(Map&);

  enum			UnitType
    {
      PLAYER = 0,
      AI,
    };

private:
  UnitType		_type;
  BombManager		_manager;
  size_t		_id;
  double		_movespeed;
  int			_shield;
  bool			_alive;
  std::pair<int, int>	_position;
  moveFunc		_moveTab[4];

  bool			_moveRight(Map&);
  bool			_moveBot(Map&);
  bool			_moveLeft(Map&);
  bool			_moveTop(Map&);

  bool			_checkElement(Map&, const std::pair<int, int>& pos);

protected:

private:
  void			seriaIdPlayer(std::string&, const size_t) const;
  void			seriaCoord(std::string&, const std::pair<int, int>&) const;
  void			seriaBombs(std::string&, const BombManager&) const;
  template<typename T>
  void			seriaStat(std::string&, const std::string&, const T) const;

public:
  AUnit(UnitType type, size_t id, const std::pair<int, int>& pos);
  AUnit(const UnitType, const BombManager&, const size_t id,
  	const double ms, const int shield, const bool status,
  	const std::pair<int, int>& pos);
  AUnit(const AUnit&);

  virtual ~AUnit() {}

  // operators
  AUnit&		operator=(const AUnit&);

  bool			changeBomb(const ABomb&);
  bool			addBomb();
  bool			addRangeBomb();
  bool			addPowerBomb();
  bool			addMoveSpeed();
  bool			addShield();

  bool			move(const Direction dir, Map& map);
  bool			putBomb(std::list<ABomb*>&);
  bool			pushBomb();
  bool			getBonus(const ABonus&);

  bool			touched();
  std::string		serialize() const;

  virtual AMapElement	*explode() const { return (NULL); };

  // GETTERS
  size_t		getID() const {return _id;}
  bool			isAlive() const {return _alive;}
};

#endif	// !AUNIT_HPP__
