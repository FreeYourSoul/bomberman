//
// BombManager.cpp for  in /home/sorman/Epitech/Tek2/c++/mybbman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Thu May  9 16:30:11 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:44:39 2013 Pierre-Nicolas Sormani
//

#include		"ABomb.hpp"
#include		"BombManager.hpp"
#include		"Bombs/StandardBomb.hpp"


BombManager::BombManager(const int id, const BombType type) :
  _typeBomb(type)
{
  _nbBombs = 2;
  _bomb = new StandardBomb(std::make_pair(-1, -1), id);
}

BombManager::BombManager(const BombManager& o)
{
  _typeBomb = o._typeBomb;
  _nbBombs = o._nbBombs;
  _bomb = (o._bomb)->clone(std::make_pair(-1, -1));
}

BombManager&			BombManager::operator=(const BombManager& o)
{
  _typeBomb = o._typeBomb;
  _nbBombs = o._nbBombs;
  _bomb = (o._bomb)->clone(std::make_pair(-1, -1));
  return *this;
}

BombManager::~BombManager()
{
  delete _bomb;
}

ABomb*				BombManager::putBomb(const std::pair<int, int>& pos)
{
  return _bomb->clone(pos);
}

bool				BombManager::changeBomb(const ABomb& b)
{
  if (_bomb->getType() == b.getType())
    return false;
  delete _bomb;
  _bomb = b.clone(std::make_pair(-1, -1));
  return true;
}

void				BombManager::addBomb()
{
  ++_nbBombs;
}

bool				BombManager::addRangeBomb()
{
  _bomb->addRange();
  return true;
}

bool				BombManager::addPowerBomb()
{
  _bomb->addPower();
  return true;
}
