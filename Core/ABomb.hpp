//
// ABomb.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May  3 15:12:14 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 10:33:12 2013 Pierre-Nicolas Sormani
//

#ifndef			ABOMB_HPP__
# define		ABOMB_HPP__

# include		"../Map/AMapElement.hh"
# include		"BombType.hpp"

# include		<cstdlib>
# include		<utility>
# include		<list>
# include		<vector>

class			AUnit;
class			Map;
class			ABomb;

class			ABomb : public AMapElement
{
public:

protected:
  BombType		_type;
  size_t		_range;
  size_t		_power;
  size_t		_playerID;
  float			_timer;
  bool			_explode;

public:
  virtual ~ABomb();
  ABomb(const std::pair<int, int>&, BombType, size_t playerID, size_t range, size_t power);
  ABomb(const ABomb&);

  // Operators
  ABomb&		operator=(const ABomb&);

  // Getters
  BombType		getType() const {return _type;}
  virtual size_t	getRange() const {return _range;}
  virtual size_t	getPower() const {return _power;}
  size_t		getPlayerID() const {return _playerID;}
  bool			getExplode() const {return _explode;}
  float			getTimer() const {return _timer;}

  // Setters
  void			setRange(size_t);
  void			setPower(size_t);
  void			setTimer(const float&);

  // other
  virtual ABomb*	clone(const std::pair<int, int>&) const = 0;
  AMapElement*		explode() {return 0;}
  virtual void		explode(Map& map, std::vector<AUnit*>& players,
				std::list<AUnit*>& killed) = 0;

  // Operations
  void			addRange();
  void			addPower();
};

#endif				// !ABOMB_HPP__
