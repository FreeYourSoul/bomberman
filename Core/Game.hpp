//
// Game.hpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 14:15:09 2013 Pierre-Nicolas Sormani
// Last update Tue May 21 11:36:44 2013 Pierre-Nicolas Sormani
//

#ifndef				GAME_HPP__
#define				GAME_HPP__

#include			"../Map/Map.hh"

#include			<utility>
#include			<vector>
#include			<list>

class				AUnit;
class				ABomb;

class				Game
{
private:
  Map				_map;
  std::vector<AUnit*>		_players;
  std::list<ABomb*>		_bombs;

  std::list<AUnit*>		_killed;

  void				_explosions();
  void				_move(AUnit*);

public:
  Game(const int x, const int y, const int humanPlayers, const int AI);
  ~Game();

  void				launch();
};

#endif	// !GAME_HPP__
