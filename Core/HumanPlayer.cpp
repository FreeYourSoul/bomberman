//
// HumanPlayer.cpp for  in /home/sorman/Epitech/Tek2/c++/bomberman/Core
//
// Made by Pierre-Nicolas Sormani
// Login   <sorman_p@epitech.net>
//
// Started on  Fri May 17 17:22:49 2013 Pierre-Nicolas Sormani
// Last update Mon May 20 17:55:31 2013 lucas maillardet
//

#include		"AUnit.hpp"
#include		"HumanPlayer.hpp"

HumanPlayer::HumanPlayer(const int id) :
  AUnit(AUnit::PLAYER, id, std::make_pair(-1, -1))
{
  std::cout << AUnit::getID() << " - A Human Player has been created." << std::endl;
}

HumanPlayer::~HumanPlayer()
{}
