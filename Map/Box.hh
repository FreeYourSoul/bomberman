//
// Box.hh for box in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Tue May  7 10:30:39 2013 lucas maillardet
// Last update Mon May 20 17:37:27 2013 lucas maillardet
//

#ifndef			BOX_HH__
# define		BOX_HH__

class			AMapElement;
class			ABonus;

class			Box : public AMapElement
{
  ABonus		*_bonus;
  bool			_destructible;
  
public:
  virtual ~Box();
  Box(const std::pair<int, int>&, ABonus *, const bool);

  virtual AMapElement	*explode();
  virtual bool		isDestructible() const;
};

#endif // !BOX_HH_
