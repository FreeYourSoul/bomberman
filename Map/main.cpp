
#include "Map.hh"

int main()
{
  Map		*map = new Map(50, 50);
  std::vector<AUnit*> units;

  for (unsigned int i = 0; i < 25; i++)
    units.push_back(new AUnit(AUnit::PLAYER, i, std::make_pair(-1, -1)));
  map->putPlayer(units);
  map->display();
}
