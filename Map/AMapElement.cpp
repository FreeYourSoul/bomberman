//
// AMapElement.cpp for element in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Tue May  7 10:02:13 2013 lucas maillardet
// Last update Mon May 20 16:42:29 2013 lucas maillardet
//

#include "AMapElement.hh"

AMapElement::~AMapElement()
{}

AMapElement::AMapElement(AMapElement::Type type, std::pair<int, int> pos) : _position(pos),
									    _type(type)
{}

std::pair<int, int>	AMapElement::getPosition() const
{
  return (_position);
}

AMapElement::Type	AMapElement::getType() const
{
  return (_type);
}

bool			AMapElement::isDestructible() const
{
  return (false);
}

int			AMapElement::getTypeElem() const
{
  return (0);
}


void			AMapElement::setPosition(std::pair<int, int> pos)
{
  _position = pos;
}

bool			AMapElement::operator==(const AMapElement & other)
{
  if (this->_position == other._position && this->_type == other._type)
    return (true);
  return (false);
}
