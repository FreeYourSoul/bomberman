//
// Map.cpp for map in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Mon May  6 16:15:43 2013 lucas maillardet
// Last update Tue May 21 12:00:05 2013 lucas maillardet
//

#include "../Core/Direction.hpp"
#include "../Core/AUnit.hpp"
#include "Box.hh"
#include "AMapElement.hh"
#include "Bonus/BonusFactory.hh"
#include "../Loader/ISerializable.hh"
#include "Map.hh"
#include <exception>

Map::~Map()
{}

Map::Map(const t_map& map, const std::pair<int, int>& size) :
  ISerializable(),
  _map(map),
  _size(size),
  _factory()
{}

Map::Map(const int width, const int height) : ISerializable()
{
  std::pair<int, int>	pair;

  srandom(time(NULL));
  _size = std::make_pair(width, height);
  for (int i = 0; i < width - 1; i++)
    {
      for (int j = 0; j < height - 1; j++)
	{
	  pair = std::make_pair(i, j);
	  if ((i % 2) != 0 && 
	      (j % 2) != 0)
	    _map[pair].push_back(new Box(pair, NULL, false));
	  else
	    _map[pair].push_back(getElement(pair));
	}
    }
}

AMapElement			*Map::getElement(const std::pair<int, int>& pos)
{
  int				per;
  static const ABonus::TypeBonus	bonus[ABonus::endbonus] = {
    ABonus::speed,
    ABonus::shield,
    ABonus::power,
    ABonus::range,
  };

  if (((random() + 1) % 100) < 70)
    {
      per = (random() + 1) % 100;
      return (new Box(pos,
		      (per < 30) ? _factory.bonusFactory(bonus[random() % 4], pos) : NULL,
		      true));
    }
  return (NULL);
}

void			Map::clearPos(const std::pair<int, int> & pos)
{
  _map[std::make_pair(pos.first, pos.second + 1)].clear();
  _map[std::make_pair(pos.first + 1, pos.second)].clear();
  _map[std::make_pair(pos.first, pos.second - 1)].clear();
  _map[std::make_pair(pos.first - 1, pos.second)].clear();
}

void			Map::putPlayer(const std::vector<AUnit*> & player)
{
  int					j = 0;
  static const	std::pair<int, int>	spawn[14] = {
    std::make_pair(0, 0),
    std::make_pair(_width - 2, 0),
    std::make_pair(_width - 2, _height - 2),
    std::make_pair(0, _height - 2),
    std::make_pair(0, (_height - 2) / 2),
    std::make_pair((_width - 2) / 2, 0),
    std::make_pair((_width - 2) / 2, (_height - 2)),
    std::make_pair((_width - 2), (_height - 2) / 2),
    std::make_pair((_width - 2) / 4, (_height - 2) / 2),
    std::make_pair((_width - 2) / 2, (_height - 2) / 4),
    std::make_pair(3 * ((_width - 2) / 4 ), (_height - 2) / 2),
    std::make_pair((_width - 2) / 2, 3 * ((_height - 2) / 4)),
    std::make_pair((_width - 2) / 2, (_height - 2) / 2),
  };
  
  for (unsigned int i = 0; i < player.size(); i++)
    {
      player[i]->setPosition(spawn[j]);
      _map[spawn[j]].clear();
      _map[spawn[j]].push_back(player[i]);
      clearPos(spawn[j]);
      j++;
      if (j == 14)
	j = 0;
    }
}

void			Map::movement(AMapElement* element, const Direction dir)
{
  std::pair<int, int>	pos;
  std::pair<int, int>	tabPos[ENDDIR] = {
    std::make_pair(element->getPosition().first + 1, element->getPosition().second),
    std::make_pair(element->getPosition().first, element->getPosition().second + 1),
    std::make_pair(element->getPosition().first - 1, element->getPosition().second),
    std::make_pair(element->getPosition().first, element->getPosition().second - 1),
  };
  
  removeElement(element);
  if (dir >= 0 && dir < ENDDIR)
    pos = tabPos[dir];
  else
    throw new std::exception;
  if (_map[pos].front()->getType() == AMapElement::BONUS)
    (reinterpret_cast<ABonus*>(_map[pos].front()))->setBonus(*(reinterpret_cast<AUnit*>(element)));
  element->setPosition(pos);
  putElement(element);
}

bool			Map::putElement(AMapElement* element)
{
  if (_map[element->getPosition()].size() == 0 || _map[element->getPosition()].front() == NULL)
    {
      _map[element->getPosition()].push_back(element);
      return (true);
    }
  return (false);
}

bool			Map::removeElement(AMapElement* element)
{
  std::vector<AMapElement*>::iterator it;

  if (element == *(_map[element->getPosition()].end()))
    {
      _map[element->getPosition()].erase(it);
      return (true);
    }
  it = std::find(_map[element->getPosition()].begin(), _map[element->getPosition()].end(), element);
  if (it != _map[element->getPosition()].end())
    {
      _map[element->getPosition()].erase(it);
      return (true);
    }
  return (false);
}



  void			Map::display()
  {
    for (int i = 0; i < _width; i++)
      {
	for (int j = 0; j < _height; j++)
	  {
	    if (_map[std::make_pair(i, j)].size() == 1 &&
		_map[std::make_pair(i, j)].front() != NULL)
	      {
		if (_map[std::make_pair(i, j)].front()->getType() == AMapElement::BOX)
		  {
		    if (_map[std::make_pair(i, j)].front()->isDestructible())
		      std::cout << "*";
		    else
		      std::cout << "o";
		  }
		else
		  if (_map[std::make_pair(i, j)].front()->getType() == AMapElement::PLAYER)
		    std::cout << "I";
	      }
	    else
	      std::cout << " ";
	  
	  }
	std::cout << std::endl;
      }
  }

  const std::pair<int, int> & Map::getSize() const
  {
    return (_size);
  }

  std::vector<AMapElement*> & Map::getElementOnPos(const std::pair<int, int> & pos)
  {
    return (_map[pos]);
  }
