//
// AMapElement.hh for element in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Tue May  7 10:02:06 2013 lucas maillardet
// Last update Mon May 20 17:56:54 2013 lucas maillardet
//

#ifndef			AMAPELEMENT_HH__
# define		AMAPELEMENT_HH__

# include <utility>

class			AMapElement
{
public:
  enum			Type
    {
      BOX,
      BOMBE,
      BONUS,
      PLAYER,
      END,
    };

protected:
  std::pair<int, int>	_position;
  AMapElement::Type	_type;

public:
  virtual ~AMapElement();
  AMapElement(AMapElement::Type type, std::pair<int, int> pos);
  std::pair<int, int>	getPosition() const;
  AMapElement::Type	getType() const;
  virtual int		getTypeElem() const;
  virtual bool		isDestructible() const;
  virtual void		setPosition(std::pair<int, int> pos);
  bool			operator==(const AMapElement &);
};

#endif //!AMAPELEMENT_HH_
