//
// Box.cpp for box in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Tue May  7 10:30:42 2013 lucas maillardet
// Last update Mon May 20 17:37:16 2013 lucas maillardet
//

#include "AMapElement.hh"
#include "Bonus/ABonus.hh"
#include "Box.hh"
#include <cstdlib>

Box::~Box()
{}

Box::Box(const std::pair<int, int>& pos, ABonus *bonus, const bool destructible) :
  AMapElement(AMapElement::BOX, pos),
  _bonus(bonus),
  _destructible(destructible)
{}

AMapElement	*Box::explode()
{
  if (isDestructible() && _bonus)
    return (reinterpret_cast<AMapElement*>(_bonus));
  return (NULL);
}

bool		Box::isDestructible() const
{
  return (_destructible);
}
