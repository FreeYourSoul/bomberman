//
// Bonus.hh for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 17:22:05 2013 francois kardous
// Last update Mon May 20 17:45:51 2013 lucas maillardet
//

#ifndef SPEEDBONUS_HH_
#define SPEEDBONUS_HH_

#include		"ABonus.hh"

class			SpeedBonus: public ABonus
{
public:
  virtual ~SpeedBonus();
  SpeedBonus(const std::pair<int, int> &);
  void			setBonus(AUnit &);
  ABonus::TypeBonus	getBonus();
};

#endif // SPEEDBONUS_HH_
