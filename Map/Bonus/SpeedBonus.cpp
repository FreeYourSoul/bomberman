//
// Bonus.cpp for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 18:10:41 2013 francois kardous
// Last update Mon May 20 17:50:16 2013 lucas maillardet
//

#include "../../Core/AUnit.hpp"
#include "ABonus.hh"
#include "SpeedBonus.hh"

SpeedBonus::~SpeedBonus()
{}

SpeedBonus::SpeedBonus(const std::pair<int, int> & pos) : ABonus(ABonus::speed, pos)
{}

void		SpeedBonus::setBonus(AUnit &pl)
{
  pl.addMoveSpeed();
}

ABonus::TypeBonus	SpeedBonus::getBonus()
{
  return (this->_type);
}
