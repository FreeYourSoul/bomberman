//
// ABonus.hh for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Tue May  7 12:12:34 2013 francois kardous
// Last update Mon May 20 17:00:40 2013 lucas maillardet
//

#ifndef			ABONUS_HH_
# define		ABONUS_HH_

# include "../../Core/AUnit.hpp"
# include "../AMapElement.hh"
# include <iomanip>

class			ABonus: public AMapElement
{
public:
  enum			TypeBonus
    {
      speed = 0,
      shield,
      power,
      range,
      endbonus,
    };

protected:
  TypeBonus		_type;

public:
  virtual ~ABonus();
  ABonus(const ABonus::TypeBonus, const std::pair<int, int>&);
  ABonus(const ABonus &);

  ABonus			&operator=(const ABonus &);
  virtual ABonus::TypeBonus	getBonus() const;
  virtual void			setBonus(AUnit &) = 0;
  AMapElement			*explode() const;
  bool				isDestructible() const;
  int				getTypeElem() const;
};

#endif //!ABONUS_HH_
