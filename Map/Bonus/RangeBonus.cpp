//
// Bonus.cpp for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 18:10:41 2013 francois kardous
// Last update Mon May 20 17:50:22 2013 lucas maillardet
//

#include "../../Core/AUnit.hpp"
#include "ABonus.hh"
#include "RangeBonus.hh"

RangeBonus::~RangeBonus()
{}

RangeBonus::RangeBonus(const std::pair<int, int> & pos) : ABonus(ABonus::range, pos)
{}

void		RangeBonus::setBonus(AUnit &un)
{
  un.addRangeBomb();
}

ABonus::TypeBonus	RangeBonus::getBonus()
{
  return (this->_type);
}
