//
// Bonus.hh for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 17:22:05 2013 francois kardous
// Last update Mon May 20 16:54:47 2013 lucas maillardet
//

#ifndef RANGEBONUS_HH_
#define RANGEBONUS_HH_

# include "ABonus.hh"

class			RangeBonus: public ABonus
{
public:
  virtual ~RangeBonus();
  RangeBonus(const std::pair<int, int> &);
  void			setBonus(AUnit &);
  TypeBonus		getBonus();
};

#endif // RANGEBONUS_HH_
