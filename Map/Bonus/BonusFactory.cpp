//
// BonusFactory.cpp for  in /home/kardou_f//Project_C++/Bomberman/BONUS
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May 17 15:52:52 2013 francois kardous
// Last update Mon May 20 17:39:52 2013 lucas maillardet
//

#include "SpeedBonus.hh"
#include "RangeBonus.hh"
#include "ShieldBonus.hh"
#include "PowerBonus.hh"
#include "ABonus.hh"
#include "BonusFactory.hh"

ABonus		*BonusFactory::bonusFactory(ABonus::TypeBonus bonus, const std::pair<int, int> & pos)
{
  static ABonus	*(BonusFactory::*witcher[4])(const std::pair<int, int> &) const = {
    &BonusFactory::createSpeedBonus,
    &BonusFactory::createRangeBonus,
    &BonusFactory::createShieldBonus,
    &BonusFactory::createPowerBonus
  };

  return ((this->*witcher[bonus])(pos));
}

ABonus		*BonusFactory::createSpeedBonus(const std::pair<int, int> & pos) const
{
  return (new SpeedBonus(pos));
}

ABonus		*BonusFactory::createShieldBonus(const std::pair<int, int> & pos) const
{
  return (new ShieldBonus(pos));
}

ABonus		*BonusFactory::createPowerBonus(const std::pair<int, int> & pos) const
{
  return (new PowerBonus(pos));
}

ABonus		*BonusFactory::createRangeBonus(const std::pair<int, int> & pos) const
{
  return (new RangeBonus(pos));
}
