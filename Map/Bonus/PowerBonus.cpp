//
// Bonus.cpp for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 18:10:41 2013 francois kardous
// Last update Mon May 20 17:49:58 2013 lucas maillardet
//

#include "../../Core/AUnit.hpp"
#include "ABonus.hh"
#include "PowerBonus.hh"

PowerBonus::~PowerBonus()
{}

PowerBonus::PowerBonus(const std::pair<int, int> & pos) : ABonus(ABonus::power, pos)
{}

void		PowerBonus::setBonus(AUnit &pl)
{
  pl.addPowerBomb();
}

ABonus::TypeBonus	PowerBonus::getBonus()
{
  return (this->_type);
}
