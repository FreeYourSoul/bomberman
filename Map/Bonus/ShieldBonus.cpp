//
// Bonus.cpp for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 18:10:41 2013 francois kardous
// Last update Mon May 20 17:50:19 2013 lucas maillardet
//

#include "../../Core/AUnit.hpp"
#include "ABonus.hh"
#include "ShieldBonus.hh"

ShieldBonus::~ShieldBonus()
{}

ShieldBonus::ShieldBonus(const std::pair<int, int> & pos) : ABonus(ABonus::shield, pos)
{}

void		ShieldBonus::setBonus(AUnit &pl)
{
  pl.addShield();
}

ABonus::TypeBonus	ShieldBonus::getBonus()
{
  return (this->_type);
}
