//
// ABonus.cpp for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Wed May  8 17:00:44 2013 francois kardous
// Last update Mon May 20 16:11:47 2013 lucas maillardet
//

#include "ABonus.hh"

ABonus::~ABonus()
{}

ABonus::ABonus(ABonus::TypeBonus typ, const std::pair<int, int> &pos) :
  AMapElement(AMapElement::BONUS, pos), _type(typ)
{}

ABonus::ABonus(const ABonus &bon) :
  AMapElement(AMapElement::BONUS, bon.getPosition()),
  _type(bon._type)
{}

ABonus		&ABonus::operator=(const ABonus &bon)
{
  if (this != &bon)
    this->_type = bon._type;
  return (*this);
}

ABonus::TypeBonus	ABonus::getBonus() const
{
  return (this->_type);
}

bool			ABonus::isDestructible() const
{
  return (true);
}

int			ABonus::getTypeElem() const
{
  return (this->_type);
}

AMapElement*		ABonus::explode() const
{
  return (NULL);
}
