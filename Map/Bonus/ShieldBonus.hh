//
// Bonus.hh for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 17:22:05 2013 francois kardous
// Last update Mon May 20 16:54:44 2013 lucas maillardet
//

#ifndef SHIELDBONUS_HH_
#define SHIELDBONUS_HH_

# include "ABonus.hh"

class			ShieldBonus: public ABonus
{
public:
  virtual ~ShieldBonus();
  ShieldBonus(const std::pair<int, int> &);
  void			setBonus(AUnit &);
  TypeBonus		getBonus();
};

#endif // SHIELDBONUS_HH_
