//
// Bonus.hh for  in /home/kardou_f//Project_C++/Bomberman
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May  3 17:22:05 2013 francois kardous
// Last update Mon May 20 16:54:49 2013 lucas maillardet
//

#ifndef POWERBONUS_HH_
#define POWERBONUS_HH_

# include "ABonus.hh"

class			PowerBonus: public ABonus
{
public:
  virtual ~PowerBonus();
  PowerBonus(const std::pair<int, int> &);
  void			setBonus(AUnit &);
  TypeBonus		getBonus();
};

#endif // POWERBONUS_HH_
