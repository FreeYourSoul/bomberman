//
// BonusFactory.hh for  in /home/kardou_f//Project_C++/Bomberman/BONUS
// 
// Made by francois kardous
// Login   <kardou_f@epitech.net>
// 
// Started on  Fri May 17 15:52:46 2013 francois kardous
// Last update Mon May 20 16:57:01 2013 lucas maillardet
//

#ifndef BONUSFACTORY_HH_
#define BONUSFACTORY_HH_

# include "SpeedBonus.hh"
# include "RangeBonus.hh"
# include "ShieldBonus.hh"
# include "PowerBonus.hh"
# include <utility>

class		ABonus;

class		BonusFactory
{
public:
  BonusFactory() {}
  ~BonusFactory() {}
  ABonus		*bonusFactory(ABonus::TypeBonus, const std::pair<int, int> &);
  
private:
  ABonus		*createSpeedBonus(const std::pair<int, int> &) const;
  ABonus		*createRangeBonus(const std::pair<int, int> &) const;
  ABonus		*createShieldBonus(const std::pair<int, int> &) const;
  ABonus		*createPowerBonus(const std::pair<int, int> &) const;

};

#endif // BONUSFACTORY_HH_
