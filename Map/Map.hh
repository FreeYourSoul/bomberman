//
// Map.hh for map in /home/mailla_d//perso/BOMBERMAN
// 
// Made by lucas maillardet
// Login   <mailla_d@epitech.net>
// 
// Started on  Mon May  6 16:15:41 2013 lucas maillardet
// Last update Tue May 21 11:59:13 2013 lucas maillardet
//

#ifndef			MAP_HH__
# define		MAP_HH__

# include "../Core/Direction.hpp"
# include "Bonus/BonusFactory.hh"
# include "AMapElement.hh"
# include "../Loader/ISerializable.hh"

# include <algorithm>
# include <iostream>
# include <utility>
# include <cstdlib>
# include <map>
# include <vector>

# define PERCENTBONUS	20

# define _width _size.first
# define _height _size.second 

typedef std::map<std::pair<int, int>, std::vector<AMapElement*> > t_map;

class				AUnit;

class				Map : public ISerializable
{
  t_map				_map;
  std::pair<int, int>		_size;
  BonusFactory			_factory;
  AMapElement *			getElement(const std::pair<int, int>&);

public:
  virtual ~Map();
  Map(const int, const int);
  Map(const t_map&, const std::pair<int, int>&);

  void				movement(AMapElement *, const Direction);
  bool				putElement(AMapElement *);
  bool				removeElement(AMapElement *);
  void				putPlayer(const std::vector<AUnit*> & players);
  const std::pair<int, int> &	getSize() const;
  std::vector<AMapElement*> &	getElementOnPos(const std::pair<int, int> & pos);
  void				display();
  std::string			serialize() const { return ("");}

private:
  void				clearPos(const std::pair<int, int> &pos);
  void				seriaSizeOrCoord(std::string&, const bool, const std::pair<int, int>&) const;
  void				seriaElem(std::string&, const AMapElement *) const;
  void				seriaMap(std::string&) const;
};

#endif // MAP_HH_
