//
// DocTypeDefDTD.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Thu May  2 21:17:07 2013 quentin balland
// Last update Thu May  9 23:12:38 2013 quentin balland
//

#include <fstream>
#include <map>
#include <vector>
#include <string>
#include "DocTypeDefDTD.hh"
#include "ParserXMLException.hh"
#include "Lexer.hh"
#include "ParserXML.hh"

DocTypeDefDTD::~DocTypeDefDTD()
{}

DocTypeDefDTD::DocTypeDefDTD(const std::string& fileDtd)
{
  fillDtd(fileDtd);
}

void		DocTypeDefDTD::fillDtd(const std::string& fileDtd)
{
  std::ifstream	file(fileDtd.c_str(), std::ios::in);
  std::string	line;
  Lexer		lexDtd;

  if (file)
    {
      while (getline(file, line))
	{
	  lexDtd.initBasical();
	  if (!line.empty() && (line[0] != '#'))
	    fillFromLine(lexDtd.launchManualLexing(line));
	}
    }
}

bool				DocTypeDefDTD::checkBaliseFormat(const std::string& balise) const
{
  for (size_t i = 0 ; i < balise.size() ; ++i)
    {
      if (!((balise.at(i) >= 'A' && balise.at(i) <= 'Z') ||
	    (balise.at(i) >= 'a' && balise.at(i) <= 'z')) && (balise.at(i) != '_'))
	return (false);
    }
  return (true);
}

#include <iostream>
void				DocTypeDefDTD::fillFromLine(const std::vector<std::string>& lineParsed)
{
  std::vector<std::string>	restreint;
  std::string			balise;
  size_t			i;

  if (lineParsed.size() != 0)
    {
      balise = lineParsed[0];
      if (dtd.count(lineParsed[0]) == 1)
	throw ParserXMLException(lineParsed[0] + ERR_DTD_REDEFI);
      else if (!checkBaliseFormat(balise))
	throw ParserXMLException(balise + ERR_DTD_FORMAT);
    }
  if (lineParsed.size() > 1)
    {      
      for (i = 1 ; i < lineParsed.size() ; ++i)
	{
	  if (((dtd.size() > 0) && (dtd.count(lineParsed[i]) == 0)) ||
	      (!(lineParsed[i].compare("c"))))
	    throw ParserXMLException(lineParsed[i] + ERR_DTD_YETDEF);
	  else if (!lineParsed[i].compare(balise))
	    throw ParserXMLException(lineParsed[i] + ERR_DTD_REDEFI);
	  else
	    restreint.push_back(lineParsed[i]);
	}
    }
  this->dtd[balise] = restreint;
}

bool				DocTypeDefDTD::isAccepted(const std::string& baliseMother,
							  const std::string& baliseSon) const
{
  std::vector<std::string>	tmp;

  if (isBaliseGood(baliseMother))
    {
      tmp = dtd.at(baliseMother);
      for (std::vector<std::string>::iterator it = tmp.begin() ; it < tmp.end() ; ++it)
	{
	  if (!((*it).compare(baliseSon)))
	    return (true);
	}
    }
  throw ParserXMLException(baliseMother + ERR_DTD_NOTDEF);
  return (false);
}

bool		DocTypeDefDTD::isBaliseGood(const std::string& balise) const
{
  if (dtd.find(balise) == dtd.end())    
    throw ParserXMLException(balise + ERR_DTD_NOTDEF);
  return (true);
}

bool		DocTypeDefDTD::findInDtd(std::vector<std::string>& cmpContent,
					 const std::string& name) const
{
  for (std::vector<std::string>::iterator it = cmpContent.begin() ; it != cmpContent.end() ; ++it)
    {
      if (!((*it).compare(name)))
	{
	  if (!(*it).compare(0, 1, "_"))
	    return (false);
	  cmpContent.erase(it);
	  return (true);
	}
    }
  return (true);
}

size_t		DocTypeDefDTD::removeInfiniteBalise(const std::vector<std::string>& cmpContent) const
{
  size_t	countInfiniteBalise;

  countInfiniteBalise = 0;
  for (std::vector<std::string>::const_iterator it = cmpContent.begin() ; it != cmpContent.end() ; ++it)
    {
      if (!(*it).compare(0, 1, "_"))
	++countInfiniteBalise;
    }
  return (cmpContent.size() - countInfiniteBalise);
}

bool		DocTypeDefDTD::isBaliseComplete(const std::vector<Parser::Node>& content,
						const std::string& baliseName) const
{
  std::vector<std::string>	compareContent;
  size_t			i;
  
  if (this->dtd.find(baliseName) == this->dtd.end())
    return (false);
  compareContent = this->dtd.at(baliseName);
  i = removeInfiniteBalise(compareContent);
  for (std::vector<Parser::Node>::const_iterator it = content.begin() ;
       it != content.end() ; ++it)
    {
      if (findInDtd(compareContent, (*it).getBaliseName()))
	--i;
    }
  return ((i == 0 && removeInfiniteBalise(compareContent) == 0) ? true : false);
}
