//
// Node.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Thu May  2 19:26:46 2013 quentin balland
// Last update Thu May  9 18:18:44 2013 quentin balland
//

#include <string>
#include <vector>
#include "ParserXML.hh"
#include "ParserXMLException.hh"

Parser::Node::~Node()
{}

Parser::Node::Node() :
  balise(""), content("")
{}

std::string	Parser::Node::getBaliseName() const
{
  return (this->balise);
}

void		Parser::Node::setBaliseName(const std::string& balise)
{
  this->balise = balise;
}

void		Parser::Node::setContent(const std::string& content)
{
  if (!content.empty())
    this->content += (this->content.empty()) ? content : (" " + content);
}

std::string	Parser::Node::getContent() const
{
  return (this->content);
}

Parser::Node	Parser::Node::getNodeAt(const int id) const
{
  if ((id < 0) ||  (id >= getNodeInsideSize()))
    throw ParserXMLException("Bad request for getNodeAt, bad id.");
  return (this->insideNode.at(id));
}

std::vector<Parser::Node>	Parser::Node::getInsideNode() const
{
  return (this->insideNode);
}

std::vector<Parser::Node>	Parser::Node::getChildren(const std::string& childName) const
{
  std::vector<Parser::Node>	childs;

  for (std::vector<Parser::Node>::const_iterator it = insideNode.begin() ;
       it != insideNode.end() ; ++it)
    {
      if (!childName.compare((*it).getBaliseName()))
	childs.push_back(*it);
    }
  return (childs);
}

Parser::Node	Parser::Node::getParent() const
{
  return (*(this->parent));
}

int		Parser::Node::getNodeInsideSize() const
{
  return ((this->insideNode.empty()) ?  -1 : this->insideNode.size());
}

void		Parser::Node::addNodeInto(const Parser::Node& newNode)
{
  this->insideNode.push_back(newNode);
}

void		Parser::Node::setChildrens(const std::vector<Parser::Node>& childs)
{
  this->insideNode = childs;
}

void		Parser::Node::setParent(Parser::Node *parent)
{
  this->parent = parent;
}
