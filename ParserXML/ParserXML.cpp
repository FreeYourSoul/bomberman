//
// ParserXML.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Thu May  2 19:06:15 2013 quentin balland
// Last update Tue May 14 18:09:40 2013 quentin balland
//

#include <cstdlib>
#include <string>
#include "ParserXML.hh"
#include "Lexer.hh"
#include "DocTypeDefDTD.hh"
#include "ParserXMLException.hh"

Parser::ParserXML::~ParserXML()
{}

Parser::ParserXML::ParserXML(const std::string& fileName, const std::string& dtdFile) :
  fileName(fileName), dtd(DocTypeDefDTD(dtdFile))
{
  Lexer		lex(this->fileName);

  this->fileLexed = lex.getFileLexed();
  this->launchXMLParsing();  
}

bool			Parser::ParserXML::isAndFillBalise(const std::string& str,
							   Parser::Node& balise) const
{ 
  if (str.size() <= 3)
    return (false);
  if (str.at(0) == '<')
    {
      if (str.at(str.size() - 1) == '>')
	{
	  balise.setBaliseName(str.substr(1, (str.size() - 2)));
	  return (true);
	}
    }
  return (false);
}

bool			Parser::ParserXML::isAndFillBalise(const std::string& str,
							   std::string *balise) const
{
  if (str.size() < 3)
    return (false);
  if (str.at(0) == '<')
    {
      if (str.at(str.size() - 1) == '>')
	{
	  *balise = str.substr(1, (str.size() - 2));
	  return (true);
	}
    }
  return (false);
}

bool			Parser::ParserXML::isCloseBalise(const std::string& str,
							 const std::string& balise) const
{
  if (str.size() < 4)
    return (false);
  if (str.at(0) == '<' && str.at(1) == '/')
    {
      if ((str.at(str.size() - 1) == '>') &&
	  !(balise.compare(str.substr(2, (str.size() - 3)))))
	return (true);
    }
  return (false);
}

bool			Parser::ParserXML::checkCloseBalise(std::vector<std::string>::iterator it,
							    const std::string& balise) const
{
  while (it != fileLexed.end())
    {
      if (isCloseBalise(*it, balise))
	return (true);
      ++it;
    }
  throw ParserXMLException(balise + ": No Closing balise.");
  return (false);
}

void			Parser::ParserXML::fillContentNode(Parser::Node& actualNode,
							   std::vector<std::string>::iterator& it) const
{  
  ++it;
  while (isCloseBalise(*it, "c") != true)
    {
      if (it == fileLexed.end())
	throw ParserXMLException(actualNode.getBaliseName() + ERR_XML_CONTEN);
      actualNode.setContent(*it);
      ++it;
    }  
}

void			Parser::ParserXML::fillNode(Parser::Node& actualNode,
						    std::vector<std::string>::iterator& it) const
{
  std::string		balise;
  
  while (++it != fileLexed.end())
    {
      if (isCloseBalise(*it, actualNode.getBaliseName()))
	{
	  if (!dtd.isBaliseComplete(actualNode.getInsideNode(), actualNode.getBaliseName()))
	    throw ParserXMLException(actualNode.getBaliseName() + ERR_XML_CONTEN);
	  return;
	}
      else if (isAndFillBalise(*it, &balise) && checkCloseBalise(it, balise))
	{
	  if (!balise.compare("c"))
	    fillContentNode(actualNode, it);
	  else if (dtd.isBaliseGood(balise) && dtd.isAccepted(actualNode.getBaliseName(), balise))
	    {
	      Parser::Node newNode;
	      newNode.setBaliseName(balise);
	      newNode.setParent(&actualNode);
	      fillNode(newNode, it);
	      actualNode.addNodeInto(newNode);
	    }
	}
      else
	throw ParserXMLException(ERR_XML_BADBAL);
    }
}

void				Parser::ParserXML::launchXMLParsing()
{
  std::vector<Parser::Node>	vectorTreeXML;
  std::string			balise;
  Parser::Node			newNode;
  
  for (std::vector<std::string>::iterator it = fileLexed.begin() ; it != fileLexed.end() ; ++it)
    {
      if (!balise.empty() && isCloseBalise(*it, balise));
      else if ((isAndFillBalise(*it, &balise) && checkCloseBalise(it, balise)))
	{
	  if (!dtd.isBaliseGood(balise))
	    throw ParserXMLException(balise + ERR_XML_BADBAL);
	  newNode.setBaliseName(balise);
	  newNode.setParent(&(this->treeXML));
	  fillNode(newNode, it);
	  vectorTreeXML.push_back(newNode);
	}
    }
  this->treeXML.setChildrens(vectorTreeXML);
}

int			Parser::ParserXML::getNumContent(const Parser::Node& node)
{
  std::string		content;

  content = node.getContent();
  for (size_t i = 0 ; i < content.size() ; ++i)
    {
      if (content.at(i) < '0' || content.at(i) > '9')
	throw ParserXMLException(content + "Error in getNumContent : not a number.");
    }
  return (::atoi(content.c_str()));
}

double			Parser::ParserXML::getFloatContent(const Parser::Node& node)
{
  std::string		content;

  content = node.getContent();
  for (size_t i = 0 ; i < content.size() ; ++i)
    {
      if (content.at(i) < '0' || content.at(i) > '9')
	throw ParserXMLException(content + "Error in getNumContent : not a number.");
    }
  return (::atof(content.c_str()));
}

Parser::Node		Parser::ParserXML::getTreeXML() const
{
  return (this->treeXML);
}
