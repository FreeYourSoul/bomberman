//
// DocTypeDefDTD.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Thu May  2 21:17:05 2013 quentin balland
// Last update Thu May  9 21:33:48 2013 quentin balland
//

#ifndef			TYPEDEFDTD_HH_
# define		TYPEDEFDTD_HH_

# include		<string>
# include		<map>
# include		<string>

namespace		Parser
{
  class			Node;
};

//
// DTD file grammar:
// SPACE : ' ', '\n'
// FIRST PARAM : Mother's Balise to define
// OTHER PARAM : Son's Balise that define the mother's balise.
//

class			DocTypeDefDTD
{

private:
  std::map<std::string, std::vector<std::string> >	dtd;

public:
  ~DocTypeDefDTD();
  DocTypeDefDTD(const std::string&);

private:
  DocTypeDefDTD();
  void			fillDtd(const std::string&);
  void			fillFromLine(const std::vector<std::string>&);
  size_t		removeInfiniteBalise(const std::vector<std::string>&) const;
  bool			findInDtd(std::vector<std::string>&, const std::string&) const;
  bool			checkBaliseFormat(const std::string&) const;

public:
  bool			isAccepted(const std::string&, const std::string&) const;
  bool			isBaliseGood(const std::string&) const;
  bool			isBaliseComplete(const std::vector<Parser::Node>&,
					 const std::string&) const;
};

#endif // !TYPEDEFDTD_HH_
