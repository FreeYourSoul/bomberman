//
// ParserXML.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Thu May  2 19:06:12 2013 quentin balland
// Last update Mon May 20 18:22:30 2013 quentin balland
//

#ifndef			PARSER_HH_
# define		PARSER_HH_

# include		<vector>
# include		<map>
# include		<string>
# include		"DocTypeDefDTD.hh"

namespace		Parser
{

  class			Node
  {

  private:
    std::string		balise;
    std::string		content;
    std::vector<Node>	insideNode;
    Node		*parent;

  public:
    ~Node();
    Node();

  public:
    std::string		getBaliseName() const;
    int			getNodeInsideSize() const;
    Node		getNodeAt(const int) const;
    std::vector<Node>	getChildren(const std::string&) const;
    std::vector<Node>	getInsideNode() const;
    Node		getParent() const;
    std::string		getContent() const;
    void		setBaliseName(const std::string&);
    void		setContent(const std::string&);
    void		addNodeInto(const Parser::Node&);
    void		setParent(Parser::Node *);
    void		setChildrens(const std::vector<Node>&);

  };

  class				ParserXML
  {

  private:
    std::vector<std::string>	fileLexed;
    std::string			fileName;
    DocTypeDefDTD		dtd;
    Node			treeXML;

  public:
    ~ParserXML();
    ParserXML(const std::string&, const std::string&);

  private:
    ParserXML();
    void		launchXMLParsing();
    void		fillNode(Parser::Node&, std::vector<std::string>::iterator&) const;
    void		fillContentNode(Parser::Node&, std::vector<std::string>::iterator&) const;
    bool		isAndFillBalise(const std::string&, Parser::Node&) const;
    bool		isAndFillBalise(const std::string&, std::string *) const;
    bool		isCloseBalise(const std::string&, const std::string&) const;
    bool		checkCloseBalise(std::vector<std::string>::iterator,
					 const std::string&) const;
  public:
    Node		getTreeXML() const;

    static int		getNumContent(const Parser::Node&);
    static double	getFloatContent(const Parser::Node&);

  };

};

#endif // !PARSER_HH_
