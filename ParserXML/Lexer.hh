//
// Lexer.hh for lol in /home/ballan_q//Desktop/tech2/C++/Plazza/plazza/src/Parsing
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Tue Apr 16 17:14:50 2013 quentin balland
// Last update Mon May  6 22:34:53 2013 quentin balland
//

#ifndef			LEXER_HH_
# define		LEXER_HH_

# include		<string>
# include		<vector>

# define IS_SEP(X)	(X == ' ' || X == '\t') ? 1 : 0

class				Lexer
{

  int				cw;
  std::vector<std::string>	fileLexed;
  std::string			lineToLex;

public:
  ~Lexer();
  Lexer();
  Lexer(const std::string&);

public:
  std::vector<std::string>	getFileLexed() const;
  std::vector<std::string>	launchManualLexing(const std::string&);
  bool				isLineToLexNotEmpty() const;
  void				initBasical();

private:
  void				launchLexing(const std::string&);
  void				lexString();
  void				countWord();

};

#endif // !LEXER_HH_
