//
// Lexer.cpp for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/ParserXML
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Mon May  6 18:39:56 2013 quentin balland
// Last update Tue May 14 18:48:58 2013 quentin balland
//

#include <fstream>
#include <vector>
#include <string>
#include "Lexer.hh"
#include "ParserXMLException.hh"

Lexer::~Lexer()
{}

Lexer::Lexer() : cw(0)
{}

Lexer::Lexer(const std::string& fileToLex) : cw(0)
{
  launchLexing(fileToLex);
}

bool				Lexer::isLineToLexNotEmpty() const
{
  size_t			i;

  i = 0;
  while (i < lineToLex.size())
    {
      if (!(IS_SEP(lineToLex[i]) || lineToLex[i] == '\n'))
	return (true);
      ++i;
    }
  return (false);
}

void				Lexer::launchLexing(const std::string& fileToLex)
{
  std::ifstream			file(fileToLex.c_str(), std::ios::in);

  if (file)
    {
      while (getline(file, lineToLex))
	{	  
	  if ((isLineToLexNotEmpty()) && (lineToLex[0] != '#'))
	    {	      
	      this->countWord();
	      this->lexString();
	      
	    }
	}
    }
  else
    throw ParserXMLException(fileToLex + ERR_LEX_OPFILE);
  file.close();
}

void				Lexer::initBasical()
{
  this->cw = 0;
  this->fileLexed.clear();
  this->lineToLex.clear();
}

void				Lexer::countWord()
{
  std::string::iterator		it;

  cw = 0;
  it = lineToLex.begin();
  if (((*it) != ' ' && (*it) != '\t') && (*it) != '\n')
    ++cw;
  for (; it != lineToLex.end() ; ++it)
    {
      if ((*it == ' ' || *it  == '\t') &&
	  ((it + 1) != lineToLex.end() &&
	   (*(it + 1) != ' ' &&
	    *(it + 1) != '\t' &&
	    *(it + 1) != '\n')))
	++cw;
    }
}

void				Lexer::lexString()
{
  std::string::iterator		it;
  int				i;
  int				size;

  i = 0;
  it = lineToLex.begin();
  while (this->cw > 0)
    {
      size = 0;
      while ((it != lineToLex.end()) && ((IS_SEP(*it)) || (*it) == '\n'))
	{
	  ++it;
	  ++i;
	}
      while ((it != lineToLex.end()) && !(IS_SEP(*it)))
	{
	  ++size;
	  ++it;
	}
      this->fileLexed.push_back(lineToLex.substr(i, size));
      i = i + size;
      --cw;
    }
}

std::vector<std::string>	Lexer::launchManualLexing(const std::string& line)
{
  this->lineToLex = line;
  this->countWord();
  this->lexString();
  return (this->fileLexed);
}

std::vector<std::string>	Lexer::getFileLexed() const
{
  return (this->fileLexed);
}
