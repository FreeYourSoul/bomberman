//
// ParserXMLException.cpp for lol in /home/ballan_q//Desktop/tech2/C++/AbstractVM/Core
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Tue Feb 12 11:34:05 2013 quentin balland
// Last update Thu May  9 16:28:19 2013 quentin balland
//

#include <sstream>
#include <string>
#include "ParserXMLException.hh"

ParserXMLException::~ParserXMLException() throw()
{}

ParserXMLException::ParserXMLException(const std::string& error) throw() : error(error)
{}

const char	*ParserXMLException::what() const throw()
{
  return (this->error.c_str());
}
