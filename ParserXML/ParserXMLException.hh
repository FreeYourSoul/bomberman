//
// ParserXMLException.hh for lol in /home/ballan_q//Desktop/tech2/C++/AbstractVM/Core
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Tue Feb 12 11:41:39 2013 quentin balland
// Last update Thu May  9 19:51:20 2013 quentin balland
//

#ifndef		PARSERXMLEXCEPTION_HH_
# define	PARSERXMLEXCEPTION_HH_

# include	<exception>
# include	<string>

# define	ERR_DTD_FORMAT	": Bad balise format: only alphabetical caractere are authorized."
# define	ERR_DTD_YETDEF	": Balise not defined yet in dtd file."
# define	ERR_DTD_NOTDEF	": Balise not defined in dtd file."
# define	ERR_DTD_REDEFI	": Balise redefined in dtd file."
# define	ERR_XML_BADBAL	": Error in XML file, balise doesn't exist."
# define	ERR_XML_CONTEN	": Error in XML file, bad balise's content."
# define	ERR_LEX_OPFILE	": Error in Lexer, openning file failed."

class		ParserXMLException : public std::exception
{

private:
  std::string	error;

public:
  virtual ~ParserXMLException() throw();
  ParserXMLException(const std::string&) throw();

  virtual const char	*what() const throw();
};

#endif //!PARSERXMLEXCEPTION_HH_
