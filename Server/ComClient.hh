//
// ComClient.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Server
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Wed May 22 13:12:08 2013 quentin balland
// Last update Wed May 22 13:18:02 2013 quentin balland
//

#ifndef				COMCLIENT_HH_
# define			COMCLIENT_HH_

# include			<string>

class				ComClient
{

  std::string			msg;

public:
  ~ComClient();
  ComClient();

public:
  void				setMsg(const std::string&);
};

#endif // !COMCLIENT_HH_
