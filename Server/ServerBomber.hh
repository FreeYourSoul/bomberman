//
// ServerBomber.hh for lol in /home/ballan_q//Desktop/tech2/C++/Bomberman/bomberman/Server
// 
// Made by quentin balland
// Login   <ballan_q@epitech.net>
// 
// Started on  Wed May 22 12:56:58 2013 quentin balland
// Last update Wed May 22 13:03:34 2013 quentin balland
//

#ifndef				SERVERBOMBER_HH_
# define			SERVERBOMBER_HH_



# include		<map>

class			ServerBomber
{
  int			*fdClient;
  int			fdSocket;


public:
  ~ServerBomber();
  ServerBomber();

public:
  void			launch_server();

};

#endif // !SERVERBOMBER_HH_
